const express = require("express")
const multer = require("multer")
const fs = require("fs")
const path = "./public/uploads"
const upload = multer({ dest: path })
const port = process.env.PORT || 3000

const app = express()

app.use(express.static(path));

app.post("/upload", upload.single("image"), (req, res) => {
   console.log("Uploaded: " + req.file.filename);
   res.send(`
    <h1>File Successfully Uploaded</h1>
    <a href="/"><button>Back</button></a><br />
    <img src=${req.file.filename} height=300px>
    `)
})
app.get("/", function (req, res) {
   let feed = ``;
   fs.readdir(path, function (err, items) {
       console.log(items);
       for (let i = 0; i < items.length; i++) {
           feed += `<img src=${items[i]} height= 150px><br />`;
       }
       res.send(
           `<h1>Welcome to Kenziegram</h1>
           <form action="/upload" method="post" enctype="multipart/form-data">
               <input type="file" name="image" />
               <input type="submit" />
               </form>
               ${feed}`
               )
   })
})
// const path = './public/uploads'; fs.readdir(path, function (err, items) { console.log(items); res.send(<h1>Welcome to Kenziegram!</h1>); });



app.set("view engine");
app.get("/", (req, res) => {
   res.render("index")
});

app.listen(port, () => console.log(`Server started on port ${port}`));